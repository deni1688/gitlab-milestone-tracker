const rest = document.querySelector(".rest");
const avatarElem = document.querySelector(".avatar");
const ctx = document.querySelector(".sprintPie");
const usernameElem = document.querySelector(".assignee");
const progressElem = document.querySelector(".progress-percent");
const progressWrapperElem = document.querySelector(".progress");
const progressBarElems = document.querySelectorAll(".progress-bar");

function popup() {
  chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
    chrome.tabs.executeScript(
      tabs[0].id,
      { file: "app.js", runAt: "document_end" },
      result => {
        const totalSprint = 2400;
        const { col_open, col_doing, col_todo, username, avatar } = result[0];
        const ongoing = col_todo + col_doing;
        const totalSprintMinusOpen = totalSprint - col_open;
        const finished = totalSprintMinusOpen - ongoing;
        const finishedPercent = percent(finished, totalSprintMinusOpen);
        const finishedPercentVal = finishedPercent >= 0 ? finishedPercent : 0;

        usernameElem.innerText = username;

        avatarElem.src = avatar;

        if (finishedPercentVal <= 100 && finishedPercentVal >= 0) {
          progressElem.innerText = finishedPercentVal;
          rest.className = "rest pt-3 d-block";
          createChart(ctx, [
            percent(col_open, totalSprint),
            percent(col_todo, totalSprint),
            percent(col_doing, totalSprint)
          ]);
        }
      }
    );
  });
}

function throttle(func, limit) {
  let inThrottle;
  return function() {
    const args = arguments;
    const context = this;
    if (!inThrottle) {
      func.apply(context, args);
      inThrottle = true;
      setTimeout(() => (inThrottle = false), limit);
    }
  };
}

function createChart(ctx, data) {
  const done = data.reduce((res, num) => (res -= num), 100).toFixed(2);

  new Chart(ctx, {
    type: "doughnut",
    data: {
      datasets: [
        {
          data: [...data, done],
          backgroundColor: ["#4E5D6C", "#f0ad4e", "#5bc0de", "#5cb85c"],
          borderWidth: 0
        }
      ],
      labels: ["Buffers", "Todos", "Doing", "Done"]
    },
    options: {
      tooltips: {
        callbacks: {
          label({ datasetIndex, index }, data) {
            return `${data.labels[index]} ${data.datasets[datasetIndex].data[index]}%`;
          }
        }
      },
      legend: {
        position: "bottom",
        labels: {
          boxWidth: 12,
          fontColor: "#fff",
          padding: 10
        }
      }
    }
  });
}

function percent(partial, total) {
  return parseFloat((partial / total) * 100).toFixed(2);
}

popup();
