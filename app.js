function app() {
	const username = document.querySelector('.search-token-assignee .value').innerText || 'John Doe';
	const avatar = document.querySelector('.avatar.s20').src;

	const boardWeights = document.querySelectorAll('.issue-count-badge > span:last-child');
	const boardTitles = document.querySelectorAll('.board-title-text');

	const weigths = Array.from(boardWeights).map(el => parseInt(el.innerText));
	const titles = Array.from(boardTitles).map(
		el => 'col_' + el.innerText.replace(' ', '').toLowerCase()
	);

	return {
		...titles.reduce((result, title, index) => ({...result, [title]: weigths[index]}), {}),
		username,
		avatar
	};
}

function getUrlParameter(name) {
	name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
	const regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
	const results = regex.exec(location.search);

	return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

app();
